import random
import sqlite3
import string
import sqlite3
import urllib.request
from bs4 import BeautifulSoup

#Checks to see if string contains numbers.
def contains_numbers(letters: str):
    output = any(x.isdigit() for x in letters)
    return output

#Checks to see if string contains capital letters.
def contains_capitals(letters: str):
    output = False
    for i in letters:
        if(i.isupper()):
            output = True
    return output

#Checks to see if string contains symbols.
def contains_symbol(letters: str):
    output = False
    for i in letters:
        if i in string.punctuation:
            output = True
    return output

#Checks the data sent as a string for the value at the given index. (Very specialized for the website "https://randomuser.me/api/".)
def string_grabber(input_string: str, find_string:str) -> str:
    index = input_string.find(find_string)
    output = input_string
    output = output[index:len(output)]
    index = output.find(':')
    output = output[index:len(output)]
    index = output.find('"')
    output = output[index+1:len(output)]
    index = output.find('"')
    return(output[0:index])

#Generates a password based on the given length and complexity.
def generate_password(length: int, complexity: int):
    letters = ''
    output = ''
    eligable = False
    #Minimum length of six letter, to prevent passwords that are too small.
    if length < 6:
        length = 6
    if complexity <= 1:
        letters = string.ascii_lowercase
        output = ''.join(random.sample(letters,length))
    elif complexity == 2:
        letters = string.ascii_lowercase+string.digits
        #Loops random password generation till it adheres to the rules given.
        while eligable == False:
            output = ''.join(random.sample(letters,length))
            if contains_numbers(output):
                eligable = True
    elif complexity == 3:
        letters = string.ascii_letters+string.digits
        while eligable == False:
            output = ''.join(random.sample(letters,length))
            if contains_numbers(output) and contains_capitals(output):
                eligable = True
    else:
        letters = string.ascii_letters+string.digits+string.punctuation
        while eligable == False:
            output = ''.join(random.sample(letters,length))
            if contains_numbers(output) and contains_capitals(output) and contains_symbol(output):
                eligable = True
    return output

#Checks the password using the same checking functions as before.
def check_password_level(password: str):
    output = 0
    if contains_numbers(password) and contains_capitals(password) and contains_symbol(password):
        output = 4
    elif contains_numbers(password) and contains_capitals(password):
        output = 3
    elif contains_numbers(password) and len(password) >= 8:
        output = 3
    elif contains_numbers(password):
        output = 2
    elif len(password) >= 8:
        output = 2
    else:
        output = 1
    return output

#Creates a user by grabbing a user from the website, and appending the data to a string, uses BeautifulSoup
def create_user(db_path: str) -> None:
    page_data = urllib.request.urlopen('https://randomuser.me/api/')
    soup = str(BeautifulSoup(page_data, "html.parser"))
    db_entry = "'"
    db_entry += string_grabber(soup, "username")
    db_entry += "', '"
    db_entry += string_grabber(soup, "first")
    db_entry += "', '"
    db_entry += string_grabber(soup, "last")
    db_entry += "', '"
    db_entry += string_grabber(soup, "email")
    db_entry += "', '"
    #Generates the password with a random length between 6 and 12 and a random complexity between 1 and 4
    db_entry += generate_password(random.randint(6,12), random.randint(1,4))
    db_entry += "'"

    #Opens connection to db
    db_connection = sqlite3.connect(db_path)
    db_cursor = db_connection.cursor()

    #Appends string to SQL statement.
    db_cursor.execute("INSERT INTO users VALUES (" + db_entry + ")")
    
    db_connection.commit()
    db_connection.close()

#Creates the DB and Table.
def initialize_db(db_name: str):
    db_connection = sqlite3.connect(db_name)
    db_cursor = db_connection.cursor()
    db_cursor.execute("""CREATE TABLE users (
                        user_name text,
                        first_name text,
                        last_name text,
                        email text,
                        password text
                        )""")
    db_connection.commit()
    db_connection.close()

#Adds users based on the given int.
def create_multiple_users(db_path: str, no_of_users: int):
    for x in range(no_of_users):
        create_user(db_path)
