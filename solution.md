Brief:
This solution allows one to generate any number of users with a random password, by pulling
them from the website https://randomuser.me/api/.

Code Explanation:
	Password - The password was generated via creating a string that contains all characters in the alphabet,
	all digits and symbols. Using said string as a sample(Based on complexity level., it procedurally pulls a 
	character from it and appends it to another string. 
	
	It then checks to see if the password adheres to the guidelines of the level it is set to be, as it is
	possible that one of the required character types (capital, digit or symbol) do not show up when they
	are pulled from the string that contains all characters. (No matter how unlikely.) If it fails, it
	generates a new password with the same parameters and loops this till it is complete.
	
	User Generating - I grab users from the website https://randomuser.me/api/ using BeautifulSoup. I convert the
	data pulled to a string as the data required is all within the one div, therefore not as easily parsable as
	I'd of liked. I made a function that checks the string for the field given and returns the next string of
	letters. (IE, searches for "Name" and return "Jeremy" from "'Name' : 'Jeremy'") I then append these values 
	to a string with "," between each of the values, this'll be used later.
	
	I finally generate a random password using the "generate_password" function and append it to the end of the
	string.
	
	I create a database by creating a "initialize" function which creates a database and table. I then open
	said database and append the string created above to the SQL statement and append the data to the DB.
	As SQL statements are read in a plaintext, making the data into a string that is appended to the statement
	made the most sense to do.
	
	In regeards of BeautifulSoup, I mainly used it as it was the only library I've worked with in the past to
	scrape data from a webpage.